<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class ApiController extends Controller
{
    public function index()
    {
      return "test";
    }
    public function getRestaurantNames(Request $request)
    {
        $latitude = 13.7563;//กำหนด ละติจูด ลองจิจูด
        $longitude = 100.5018;//กำหนด ละติจูด ลองจิจูด
        $radius = 5000;//ระยะที่ใช้ค้นหา
        $types = 'restaurant';//ประเภทการค้รหา
        $keyword = $request->query('keyword');//คำสำหรับใช้ค้นหา
    
        $apiKey = env('GOOGLE_MAPS_API_KEY'); //นำค่า GOOGLE_MAPS_API_KEY ที่เก็บไว้ใน .env มาใช้
    
        if ($keyword) { //ถ้ามีการกรอก keyword มา เราจะส่งค่ากลับไปแบบตรงๆ
            $response = Http::get('https://maps.googleapis.com/maps/api/place/nearbysearch/json', [
                'location' => $latitude . ',' . $longitude,
                'radius' => $radius,
                'types' => $types,
                'keyword' => $keyword,
                'key' => $apiKey,
            ]);
    
            if ($response->successful()) {
                $data = $response->json();
            } else {
                $data = ['error' => $response->json('error_message')];
            }
        } else { //ถ้าไม่มีการกรอก keyword มา เราจะเช็คจาก Cache ก่อนว่ามีค่าไหม ถ้าไม่มีค่อยเรียกข้อมูลใหม่จาก google
            $data = Cache::remember('res', 3600, function () use ($latitude, $longitude, $radius, $types, $apiKey) {
                $response = Http::get('https://maps.googleapis.com/maps/api/place/nearbysearch/json', [
                    'location' => $latitude . ',' . $longitude,
                    'radius' => $radius,
                    'types' => $types,
                    'key' => $apiKey,
                ]);
    
                if ($response->successful()) {
                    return $response->json();
                } else {
                    return ['error' => $response->json('error_message')];
                }
            });
        }
    
        return $data;
    }

}
